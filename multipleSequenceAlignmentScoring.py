# -*- coding: utf-8 -*-
"""
Created on Tue Mar 14 13:34:20 2017

@author: danit
"""
from collections import Counter


class MultipleSequenceAlignmentScoring():
    def fasta(self, fichero):

        identificador = ''
        secuencia = ''
        palabras = []
        for line in open(fichero):  
            if line[0] == '>':
                if identificador != '':
                    palabras.append(secuencia) 
                line = line.split("\n")  
                identificador = line[0].rstrip() 
                secuencia = ''  
            else:
                secuencia = secuencia + line.rstrip() 
        palabras.append(secuencia)
        return palabras 
    
    def percentage_of_totally_conserved_columns(self, fichero):
        multipleSequenceAlignmentScoring = MultipleSequenceAlignmentScoring()
        lista = multipleSequenceAlignmentScoring.fasta(fichero)
        NRep = []
        dicFrecPos = {}
        cnt = Counter()
        cnt2 = 0
        maxNumCom = 0
        for sec1 in lista:
            for sec2 in lista:
                if sec1 != sec2:
                    for i, c1 in enumerate(sec1):
                        for j, c2 in enumerate(sec2):
                            if c1 == c2 and i == j:
                                NRep.append(i)
        maxNumCom = len(lista)
        maxNumCom = (maxNumCom * maxNumCom) - maxNumCom
        for ncoin in NRep:
            cnt[ncoin] += 1  
        dicFrecPos = cnt
        for frec in dicFrecPos.values():
            if frec == maxNumCom:
                cnt2 = 1 + cnt2
        return cnt2

    def percentage_of_non_gaps(self, fichero):
        cnt = 0
        lista = self.fasta(fichero)
        ngaps = []
        for elemento in lista:
            cnt = 0
            for i, c1 in enumerate(elemento):
                if c1[0] != '-':
                    cnt = cnt + 1
            cnt = cnt / len(elemento)
            ngaps.append(cnt)
        return ngaps

multipleSequenceAlignmentScoring = MultipleSequenceAlignmentScoring()

col_cons = multipleSequenceAlignmentScoring.percentage_of_totally_conserved_columns("Datos.FASTA")
per_ngap = multipleSequenceAlignmentScoring.percentage_of_non_gaps("Datos.FASTA")

print ("Número de coincidencias: " + str(col_cons))
print ("Porcentaje de no-gaps: " + str(per_ngap))